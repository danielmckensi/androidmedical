package com.example.myapplication

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isEmpty
import com.example.Speciality
import com.example.SpecialityRes
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() , AdapterView.OnItemSelectedListener {



    lateinit var service: ApiMedical

    var specialities : ArrayList<Speciality> = ArrayList()
    var specialitiesNames : MutableList<String> = mutableListOf()
    val documentTypes = arrayListOf("Cédula", "Tarjeta de Identidad")
    var documentTypeSelected : String? = ""
    var spinnerViewTypeSpeciality : Spinner? = null
    var editTextNameSpecialist : EditText? = null
    var editTextNames : EditText? = null
    var spinnerTextTypeDocument: Spinner? = null
    var editTextNumberDocument: EditText? = null
    var editTextDate: EditText? = null
    var editTextEmail : EditText? = null
    var editTextPhone : EditText? = null
    var editTextCountry : EditText? = null
    var editTextCity : EditText? = null
    var editTextComments: EditText? = null
    var specialistTypeSelectede : String? = ""
    var aa : ArrayAdapter<String>? = null
    var spinnerArrayAdapter : ArrayAdapter<String>? = null
    var buttonFindDocument : Button? = null
    var dateSelectedTime : Long? = 0
    var userInfo : UserInfoData? = null
    var buttonSendAppointment : Button? = null
    var dialogRequest : AlertDialog.Builder? = null
    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        spinnerViewTypeSpeciality = findViewById(R.id.spinnerTextTypeSpecialist) as Spinner
        editTextNameSpecialist = findViewById(R.id.editTextNameSpecialist) as EditText
        editTextNumberDocument = findViewById(R.id.editTextDocumentNumber) as EditText
        editTextNames = findViewById(R.id.editTextCompleteName) as EditText
        spinnerTextTypeDocument = findViewById(R.id.spinnerTextDocumentType) as Spinner
        editTextDate = findViewById(R.id.editTextDate) as EditText
        editTextEmail = findViewById(R.id.editTextEmail) as EditText
        editTextPhone = findViewById(R.id.editTextCellphone) as EditText
        editTextCountry = findViewById(R.id.editTextCountry) as EditText
        editTextCity = findViewById(R.id.editTextCity) as EditText
        editTextComments = findViewById(R.id.editTextComments) as EditText
        buttonFindDocument = findViewById(R.id.buttonFind) as Button
        buttonSendAppointment = findViewById(R.id.buttonSave) as Button
        dialogRequest = AlertDialog.Builder(this)
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://ips-project-247214.appspot.com/services/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        service = retrofit.create<ApiMedical>(ApiMedical::class.java)

        val dialogCalendar = AlertDialog.Builder(this)
        val dialogCalendarView = layoutInflater.inflate(R.layout.calendar_view, null)
        val buttonSave = dialogCalendarView.findViewById<Button>(R.id.buttonSave)
        dialogCalendar.setView(dialogCalendarView)
        dialogCalendar.setCancelable(false)
        val dialogCustom = dialogCalendar.create()
        val calendar = dialogCalendarView.findViewById<CalendarView>(R.id.calendarViewDate)
        val buttonSaveDate = dialogCalendarView.findViewById<Button>(R.id.buttonSaveDate)

        spinnerViewTypeSpeciality!!.setOnItemSelectedListener(this)
        aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, specialitiesNames)
        aa!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerViewTypeSpeciality!!.setAdapter(aa)

        spinnerTextTypeDocument!!.setOnItemSelectedListener(this)
        spinnerArrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, documentTypes)
        spinnerArrayAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerTextTypeDocument!!.setAdapter(spinnerArrayAdapter)
        var dialog = AlertDialog.Builder(this)
        //val calendar = dialogCalendarView.findViewById<CalendarView>(R.id.calendarViewDate)

        buttonSaveDate.setOnClickListener {
            dialogCustom.dismiss()
            val date = calendar.date
            val formatDate = SimpleDateFormat("dd/MM/yyyy")
            val dateString = formatDate.format(date)
            dateSelectedTime = date
            editTextDate?.setText(dateString)
        }

        buttonFind.setOnClickListener {
            if (editTextNumberDocument!!.text.toString() != ""){
                val documentNumberString = editTextDocumentNumber.text.toString()
                val documentNumberInt = documentNumberString.toInt()
                requestUser(documentNumberInt)
            }else{
                Log.i("FIeld empty", "Campo vacio")
            }

        }



        editTextDate!!.setOnClickListener {
            dialogCustom.show()
        }

        buttonSendAppointment?.setOnClickListener {
            if (validateField() == true) {
                requestSendAppointment()
            }else{
                Log.i("Error", "Campos sin completar")

                dialog.setTitle("Campos sin completar")
                dialog.setPositiveButton(android.R.string.yes) { dialog, which ->
                    Toast.makeText(applicationContext,
                        android.R.string.yes, Toast.LENGTH_SHORT).show()
                }
            }
            dialog.show()
        }
        getSpeciality()
    }

    override fun onNothingSelected(arg0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(arg0: AdapterView<*>?, arg1: View?, position: Int, id: Long) {
        if (arg0?.id == R.id.spinnerTextTypeSpecialist){
            specialistTypeSelectede = specialitiesNames[position]
        }else{
            documentTypeSelected = documentTypes[position]
        }

    }

    fun getSpeciality(){

       this.service.getSpeciality().enqueue(object: Callback<SpecialityRes>{
           override fun onFailure(call: Call<SpecialityRes>, t: Throwable) {
               Log.i("failed", "NO junciono")
           }

           override fun onResponse(call: Call<SpecialityRes>, response: Response<SpecialityRes>) {
               Log.i("success", "funcionoooo")
               specialities = response.body()?.data?.specialities ?: ArrayList()
               for (speciality in specialities){
                   specialitiesNames.add(speciality.name)
               }
               aa?.notifyDataSetChanged()
           }

       })
    }

    fun requestUser(id: Int){


        this.service.getUserInfo(id).enqueue(object: Callback<UserInfoRes>{
            override fun onFailure(call: Call<UserInfoRes>, t: Throwable) {
                Log.i("failed", "NO junciono")
            }

            override fun onResponse(call: Call<UserInfoRes>, response: Response<UserInfoRes>) {
                Log.i("success", "funcionoooo")
               if(response.body()?.code ?: "" != "CONFLICT"){
                   val user = response.body()?.data
                   userInfo = user
                   editTextNames?.setText(user?.name)
                   editTextNumberDocument?.setText(user?.document)
                   editTextPhone?.setText(user?.phoneNumber)
                   editTextCity?.setText(user?.city)


                   if(user?.birthdate != null && user?.birthdate != ""){
                       val date = user?.birthdate?.toLong() ?: 0
                       val formatDate = SimpleDateFormat("dd/MM/yyyy")
                       val dateString = formatDate.format(date)
                       editTextDate?.setText(dateString)
                   }
               }
            }

        })
    }

    fun requestSendAppointment(){

        val especialitySelected = spinnerViewTypeSpeciality?.selectedItem
        val documentTypeSelected = spinnerTextDocumentType?.selectedItem

        val specialtySelected = specialities.find{it.name == especialitySelected}

       val currentDate = Calendar.getInstance().time
        val dateLong = currentDate.time
        val infoReq = AppointmentReq()
        infoReq.company = 1
        infoReq.dateAssigment = dateSelectedTime?.toInt() ?: 0
        infoReq.dateQualifiedAppointment = dateLong.toInt()
        infoReq.epsId = 10
        infoReq.specialty = specialtySelected?.id ?: 10
        infoReq.patient = userInfo?.id ?: 10
        infoReq.professional = 10

        this.service.requestAppointment(infoReq).enqueue(object: Callback<AppointmentRes> {
            override fun onFailure(call: Call<AppointmentRes>, t: Throwable) {
                Log.i("Failed", "Fallo creacion de la cita")
                dialogRequest?.setTitle("Fallo creacion de la cita")
                dialogRequest?.setPositiveButton(android.R.string.yes) { dialog, which ->
                    Toast.makeText(applicationContext,
                        android.R.string.yes, Toast.LENGTH_SHORT).show()
                }
                dialogRequest?.show()
            }

            override fun onResponse(call: Call<AppointmentRes>, response: Response<AppointmentRes>) {
               Log.i("Success", "Creacíon de la cita exitosa")
                dialogRequest?.setTitle("Creacíon de la cita exitosa")
                dialogRequest?.setPositiveButton(android.R.string.yes) { dialog, which ->
                    Toast.makeText(applicationContext,
                        android.R.string.yes, Toast.LENGTH_SHORT).show()
                }
                dialogRequest?.show()
            }

        })


    }

    fun validateField(): Boolean{
        var validation = true

        if (spinnerViewTypeSpeciality?.isEmpty()!!){
            validation = false
        }

        if (editTextNameSpecialist?.text.toString() == ""){
            validation = false
        }

        if (editTextNames?.text.toString() == ""){
            validation = false
        }

        if (spinnerTextTypeDocument?.isEmpty()!!){
            validation = false
        }

        if (editTextNumberDocument?.text.toString() == ""){
            validation = false
        }

        if (editTextDate?.text.toString() == ""){
            validation = false
        }

        if (editTextEmail?.text.toString() == ""){
            validation = false
        }

        if (editTextPhone?.text.toString() == ""){
            validation = false
        }

        if (editTextCountry?.text.toString() == ""){
            validation = false
        }

        if (editTextCity?.text.toString() == ""){
            validation = false
        }

        return validation
    }

    fun cleanAllFields(){

    }
}
