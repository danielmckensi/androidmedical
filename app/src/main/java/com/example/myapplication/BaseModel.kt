package com.example.myapplication

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
open class BaseModel{

    @SerializedName("code")
    @Expose
    var code : String = ""

    @SerializedName("message")
    @Expose
    val message : String = ""

}