package com.example.myapplication

import com.example.Speciality
import com.example.SpecialityData
import com.example.myapplication.BaseModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AppointmentRes : BaseModel(){
    @SerializedName("data")
    @Expose
    lateinit var data : AppointmentData
}

class AppointmentData{

    @SerializedName("id")
    @Expose
    var id : Int = 0

    @SerializedName("dateQualifiedAppointment")
    @Expose
    var dateQualifiedAppointment : String = ""

    @SerializedName("dateAssigment")
    @Expose
    var dateAssigment : String = ""

    @SerializedName("professional")
    @Expose
    var professional : Int = 0

    @SerializedName("specialty")
    @Expose
    var specialty : Specialty = Specialty()

    @SerializedName("patient")
    @Expose
    var patient : Patient = Patient()

    @SerializedName("epsId")
    @Expose
    var epsId : Int = 0



}

class Company{
    @SerializedName("id")
    @Expose
    var id : Int = 0
}

class Specialty{
    @SerializedName("id")
    @Expose
    var id : Int = 0
}

class Patient{
    @SerializedName("id")
    @Expose
    var id : Int = 0
}