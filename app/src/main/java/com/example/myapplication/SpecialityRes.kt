package com.example

import com.example.myapplication.BaseModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

public class SpecialityRes: BaseModel() {
    @SerializedName("data")
    @Expose
    lateinit var data : SpecialityData

}

class SpecialityData{

    @SerializedName("companyId")
    @Expose
    var companyId : Int = 0

    @SerializedName("specialties")
    @Expose
    lateinit var specialities : ArrayList<Speciality>
}


class Speciality{

    @SerializedName("id")
    @Expose
    var id : Int = 0

    @SerializedName("name")
    @Expose
    val name : String = ""

}