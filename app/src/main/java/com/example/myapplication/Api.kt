package com.example.myapplication

import com.example.SpecialityRes
import retrofit2.Call
import retrofit2.http.*
import java.util.*

public interface ApiMedical{

    @GET("specialtylist/companyid/1")
    fun getSpeciality(): Call<SpecialityRes>

    @GET("patient/documentnumber/{id}")
    fun getUserInfo(@Path("id") id: Int?): Call<UserInfoRes>

    /*@POST("appointment")
    fun requestAppointment(@Field("dateQualifiedAppointment") dateQualifiedAppointment: Int,
                           @Field("dateAssigment") dateAssigment: Int,
                           @Field("company") company: Int,
                           @Field("professional") professional: Int,
                           @Field("specialty") specialty: Int,
                           @Field("patient") patient: Int,
                           @Field("epsId") epsId: Int
                           ): Call<AppointmentRes>*/

    @POST("appointment")
    fun requestAppointment(@Body request: AppointmentReq): Call<AppointmentRes>

}
