package com.example.myapplication

import com.example.Speciality
import com.example.myapplication.BaseModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class AppointmentReq{

    @SerializedName("dateQualifiedAppointment")
    @Expose
    var dateQualifiedAppointment : Int = 0

    @SerializedName("dateAssigment")
    @Expose
    var dateAssigment : Int = 0

    @SerializedName("company")
    @Expose
    var company : Int = 0

    @SerializedName("professional")
    @Expose
    var professional : Int = 0

    @SerializedName("specialty")
    @Expose
    var specialty : Int = 0

    @SerializedName("patient")
    @Expose
    var patient : Int = 0

    @SerializedName("epsId")
    @Expose
    var epsId : Int = 0





}