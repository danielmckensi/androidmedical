package com.example.myapplication

import androidx.recyclerview.widget.LinearLayoutManager

import android.content.Context
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.Speciality

class SpecialityDialogView(context: Context) : LinearLayout(context) {

    var specialitis = ArrayList<Speciality>()
    var adapter = CustomAdapter(specialitis)

    init{
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.especiality_dialog_view, this, true)

        val recycler = findViewById<RecyclerView>(R.id.recycler_especiality)
       // val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL)

        recycler.adapter = adapter
    }

    fun reloadData(){
        this.adapter.notifyDataSetChanged()
    }

}