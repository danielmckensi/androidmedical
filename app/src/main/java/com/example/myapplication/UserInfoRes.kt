package com.example.myapplication


import com.example.Speciality
import com.example.SpecialityData
import com.example.myapplication.BaseModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

public class UserInfoRes: BaseModel() {
    @SerializedName("data")
    @Expose
    lateinit var data : UserInfoData

}

class UserInfoData{

    @SerializedName("id")
    @Expose
    var id : Int = 0

    @SerializedName("name")
    @Expose
    var name : String = ""

    @SerializedName("document")
    @Expose
    var document : String = ""

    @SerializedName("birthdate")
    @Expose
    var birthdate : String = ""

    @SerializedName("phoneNumber")
    @Expose
    var phoneNumber : String = ""

    @SerializedName("city")
    @Expose
    var city : String = ""

    @SerializedName("gender")
    @Expose
    var gender : String = ""


}